local keymap = vim.keymap

local opts = { noremap = true, silent = true }

--<Directory Navigation>--
keymap.set("n", "<Space>o", ":NvimTreeFocus<CR>", opts)
keymap.set("n", "<Space>e", ":NvimTreeToggle<CR>", opts)

--<Window Navigation>--
keymap.set("n", "<C-h>", "<C-w>h", opts)
keymap.set("n", "<C-l>", "<C-w>l", opts)
keymap.set("n", "<C-j>", "<C-w>j", opts)
keymap.set("n", "<C-k>", "<C-w>k", opts)

--<Window Resize>--
keymap.set("n", "<C-up>", ":resize +2<CR>", opts)
keymap.set("n", "<C-Down>", ":resize -2<CR>", opts)
keymap.set("n", "<C-Left>", ":vertical resize +2<CR>", opts)
keymap.set("n", "<C-Right>", ":vertical resize -2<CR>", opts)

--<Toggle Line Numbers>--
keymap.set("n", "<Left>", ":set nornu<CR>: set nonu<CR>", opts)
keymap.set("n", "<Right>", ":set rnu<CR>:set nu<CR>", opts)

--<Save/Quit Help>--
keymap.set("n", "<C-s>", ":write<CR>", opts)
keymap.set("n", "<C-x>", ":q<CR>", opts)
keymap.set("n", "<C-A-x>", ":wqa<CR>", opts)

--<jj = Escape in "i">--
keymap.set("i", "jj", "<Escape>", opts)

--<Capital S for Sub Command>--
keymap.set("n", "<S-s>", ":%s/", { noremap = true })

--<Comment Plugin Binds>--
keymap.set("n", "<C-g>", "gcc", { noremap = false })
keymap.set("v", "<C-g>", "gcc", { noremap = false })
-->Indents
  keymap.set("v", "<", "<gv", opts)
  keymap.set("v", ">", ">gv", opts)
--<Directory Navigation>--
keymap.set("n", "<leader>t", ":TransparentToggle<CR>", opts)
