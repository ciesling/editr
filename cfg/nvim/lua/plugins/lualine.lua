local config = function()
local theme = require("lualine.themes.nightfox")
-- theme.normal.a.bg = '#71839b'
-- theme.normal.b.bg = '#39506d'
-- theme.normal.c.bg = '#192330'

local function cross()
  return [[󰳶]]
end
-- theme.normal.c.bg = nil

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = theme,
    section_separators = { left = '', right = '' },
    component_separators = { left = '', right = ''},
    globalstatus = true,
  },
  sections = {
  lualine_a = { { 'filename', path = 1, separator = { left = '', right = '' }, right_padding = 2 } },
  lualine_c = {
    'buffers', --[[ add your center compoentnts here in place of this comment ]]
  },
  lualine_x = {},
  lualine_y = {},
  lualine_z = {
    { cross, separator = { left = '', right = '' }, left_padding = 2 },
  },
},
inactive_sections = {
  lualine_a = { 'filename' },
  lualine_b = {},
  lualine_c = { 'buffers' },
  lualine_x = {},
  lualine_y = {},
  lualine_z = { cross },
},
tabline = {},
extensions = {},
}
end
return {
  "nvim-lualine/lualine.nvim",
  lazy = false,
  config = config,
}
